FROM mhart/alpine-node:10
MAINTAINER Matthijs van der Kroon <matthijs@primed.io>
LABEL Description="PrimedIO PrimedJS test container" Vendor="PrimedIO" Version="2.1.0"

RUN apk update \
 && apk upgrade \
 && apk add --no-cache bash curl dbus firefox-esr fontconfig python ttf-freefont xvfb \
 && rm  -rf /tmp/* /var/cache/apk/*

# Add gecko driver
ARG GECKODRIVER_VERSION=0.15.0
ARG GECKODRIVER_FILE=v${GECKODRIVER_VERSION}/geckodriver-v${GECKODRIVER_VERSION}-linux64.tar.gz
RUN curl -s -o /tmp/geckodriver.tar.gz -L \
	https://github.com/mozilla/geckodriver/releases/download/$GECKODRIVER_FILE \
	&& rm -rf /usr/bin/geckodriver \
	&& tar -C /usr/bin -zxf /tmp/geckodriver.tar.gz \
	&& rm /tmp/geckodriver.tar.gz \
	&& mv /usr/bin/geckodriver /usr/bin/geckodriver-$GECKODRIVER_VERSION \
	&& chmod 755 /usr/bin/geckodriver-$GECKODRIVER_VERSION \
	&& ln -fs /usr/bin/geckodriver-$GECKODRIVER_VERSION /usr/bin/geckodriver

WORKDIR /usr/src/app
COPY . .

# fix "Error: GDK_BACKEND does not match available displays": https://unix.stackexchange.com/a/9118
RUN Xvfb :19 -screen 0 1024x768x16 &
ENV DISPLAY=:19

RUN yarn install\
 && yarn global add gulp-cli

ENTRYPOINT ["sh"]