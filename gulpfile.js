/*jshint esversion: 6 */
const gulp = require('gulp');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const rename = require("gulp-rename");
const uglify = require('gulp-uglify-es').default;
const Server = require('karma').Server;


gulp.task('assemble', function() {
  return gulp.src(['./lib/*', './src/*.js'])
    .pipe(concat('primedio.js'))
    .pipe(gulp.dest('build'));
});

gulp.task("uglify", function() {
  return gulp.src(['./lib/*', './src/*.js'])
    .pipe(concat("primedio.min.js"))
    .pipe(uglify( /* options */ ))
    .pipe(gulp.dest("build/"));
});


/**
 * Watch for file changes and re-run tests on each change
 */
gulp.task('tdd', function(done) {
  new Server({
    configFile: __dirname + '/karma.conf.js'
  }, done).start();
});

// executes only unit tests
gulp.task('unit', function(done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true,
    files: ['./lib/*.js', './src/*.js', './test/unit.*.spec.js']
  }, done).start();
});

// executes only unit tests
gulp.task('it', function(done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true,
    files: ['./lib/*.js', './src/*.js', './test/sha512.js', './test/it.*.spec.js']
  }, done).start();
});

gulp.task('default', gulp.series(['tdd']), function() {console.log("done");});
gulp.task('test', gulp.series(['unit', 'it']), function() {console.log("done");});
gulp.task('build', gulp.series(['assemble', 'uglify']), function() {console.log("done");});