#!/usr/bin/env bash
if [ -z $1 ]; then echo "you must provide a version number"; exit 1; fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

gsed -i 's/"version": ".*",/"version": "'$1'",/g' $DIR/package.json
gsed -i 's/"version": ".*",/"version": "'$1'",/g' $DIR/bower.json
gsed -i 's/\/\/     primedio.js .*/\/\/     primedio.js '$1'/g' $DIR/src/primedio.js
gsed -i "s/self.VERSION = '.*';/self.VERSION = '$1';/g" $DIR/src/primedio.js
gsed -i 's/ Version=".*"/ Version="'$1'"/g' $DIR/Dockerfile