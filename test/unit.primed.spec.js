/*jshint esversion: 6 */

const expect = chai.expect;

const connectionString = window.__env__.PRIMEDIO_CONNECTIONSTRING || "NONE";
const trackingConnectionString = window.__env__.PRIMEDIO_TRACKING_CONNECTIONSTRING || "NONE";
const publicKey = window.__env__.PRIMEDIO_APIKEY_PUBKEY || "NONE";
const secretKey = window.__env__.PRIMEDIO_APIKEY_SECRETKEY || "NONE";
const username = window.__env__.PRIMEDIO_AUTH_USERNAME || "NONE";
const password = window.__env__.PRIMEDIO_AUTH_PASSWORD || "NONE";

describe('unit', function() {
    describe('mergeDeep', function() {
        it('should merge objects recursively', function() {
            const nonce = Math.floor(new Date().getTime() / 1000);

            const pio = PRIMED({
                connectionString: connectionString,
                publicKey: publicKey,
                signature: 'testsignature',
                nonce: 123456
            });

            // merge with empty 'right'
            var merged = pio.__testonly__.mergeDeep({a: 1}, {});
            expect(merged).to.deep.equal({a: 1});

            // merge with disjoint right object (union)
            merged = pio.__testonly__.mergeDeep({a: 1, b: 2}, {c: 3});
            expect(merged).to.deep.equal({a: 1, b: 2, c:3});

            // merge with joint right object: take right
            merged = pio.__testonly__.mergeDeep({a: 1, b: 2}, {b: 3});
            expect(merged).to.deep.equal({a: 1, b: 3});

            // merge with nested left and right
            merged = pio.__testonly__.mergeDeep({a: 1, b: {c: 2}}, {b: {c: 3}});
            expect(merged).to.deep.equal({a: 1, b: {c: 3}});
        });
    });

    describe('uuidv4', function() {
        it('should return uuidv4 compliant string', function() {

            const pio = PRIMED({
                connectionString: connectionString,
                publicKey: publicKey,
                signature: 'testsignature',
                nonce: 123456
            });

            const uuidv4 = pio.__testonly__.uuidv4();

            // found here: https://github.com/lil-js/uuid/blob/master/uuid.js
            const valid = /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(uuidv4);
              
            expect(valid).to.be.true;
        });
    });

    describe('dictToQueryParams', function() {
        it('should convert a dictionary to valid url parameters', function() {

            const pio = PRIMED({
                connectionString: connectionString,
                publicKey: publicKey,
                signature: 'testsignature',
                nonce: 123456
            });

            const urlparams = pio.__testonly__.dictToQueryParams({
                name: 'abc',
                age: 32,
                props: {
                    a: 123,
                    b: 'xyz'
                }
            });

            expect(urlparams).to.equal('name=abc&age=32&props=%7B%22a%22%3A123%2C%22b%22%3A%22xyz%22%7D');
        });
    });

    describe('getArgs', function() {
        it('should extract arg names from functions', function() {

            const pio = PRIMED({
                connectionString: connectionString,
                publicKey: publicKey,
                signature: 'testsignature',
                nonce: 123456
            });

            const args = pio.__testonly__.getArgs(function(now, that) {});

            expect(args).to.eql(['now', 'that']);
        });
    });

    describe('primedjs', function() {
        it('pubkey and signature should be set', function() {

            const pio = PRIMED({
                connectionString: connectionString,
                publicKey: publicKey,
                signature: 'testsignature',
                nonce: 123456
            });

            expect(pio.connectionString).to.equal(connectionString);
            expect(pio.publicKey).to.equal(publicKey);
            expect(pio.signature).to.equal('testsignature');
            expect(pio.nonce).to.equal(123456);
        });

        it('accepts custom detectors as fix values', function() {

            const pio = PRIMED({
                connectionString: connectionString,
                publicKey: publicKey,
                signature: 'testsignature',
                nonce: 123456
            });

            pio.detectors.bind({
                userId: 'myuserid'
            });

            expect(pio.detectors.get('userId')).to.equal('myuserid');
        });

        it('should accept custom detectors as functions', function() {

            const pio = PRIMED({
                connectionString: connectionString,
                publicKey: publicKey,
                signature: 'testsignature',
                nonce: 123456
            });

            const testfn = function(now) {
                const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                const timebins = ["00-03", "03-06", "06-09", "09-12", "12-15", "15-18", "18-21", "21-24"];

                return days[now.getDay()] + "_" + timebins[Math.floor(now.getHours() / 3)];
            };

            pio.detectors.bind({
                toddow: testfn
            });

            const expected = testfn(pio.features.get('now'));
            const actual = pio.detectors.all().toddow;

            expect(expected).to.equal(actual);
        });
    });
});