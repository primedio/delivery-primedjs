/*jshint esversion: 6 */

const expect = chai.expect;

const postAjax = function({
    url,
    user,
    password
}) {
    return new Promise(
        function(resolve, reject) {
            xml = new XMLHttpRequest();
            xml.open("POST", url);
            xml.onreadystatechange = function() {
                const self = this;
                if (self.readyState == 4 & (self.status < 200 | self.status > 204)) {
                    reject(self);
                }
                if (self.readyState == 4 & self.status >= 200 & self.status <= 204) {
                    resolve(self);
                }
            };
            xml.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + password));
            xml.setRequestHeader("Content-Type", "application/json");
            xml.send(null);
        }
    );
};

const connectionString = window.__env__.PRIMEDIO_CONNECTIONSTRING;
const trackingConnectionString = window.__env__.PRIMEDIO_TRACKING_CONNECTIONSTRING;
const publicKey = window.__env__.PRIMEDIO_APIKEY_PUBKEY;
const secretKey = window.__env__.PRIMEDIO_APIKEY_SECRETKEY;
const username = window.__env__.PRIMEDIO_AUTH_USERNAME;
const password = window.__env__.PRIMEDIO_AUTH_PASSWORD;

describe('it', function() {
    before(function(done) {
        this.timeout(15000); // the db is cold, this may take a while

        postAjax({
                url: connectionString + '/api/v1/testhelper/setup',
                user: username,
                password: password
            })
            .then((obj) => {
                done();
            })
            .catch(function(obj) {
                console.error(obj);
                done(obj);
            });
    });

    after(function(done) {
        this.timeout(15000); // the db is cold, this may take a while

        postAjax({
                url: connectionString + '/api/v1/testhelper/teardown',
                user: username,
                password: password
            })
            .then((obj) => {
                done();
            })
            .catch(function(obj) {
                console.error(obj);
                done(obj);
            });
    });

    describe('should perform personalise calls', function() {
        it('should return results for __CONTROL__', function(done) {
            const nonce = Math.floor(new Date().getTime() / 1000);

            const pio = PRIMED({
                nonce: nonce,
                publicKey: publicKey,
                signature: sha512(publicKey + "" + secretKey + "" + nonce),
                connectionString: connectionString
            });

            pio.personalise({
                    campaign: "mycampaign",
                    limit: 3,
                    abvariant: 'CONTROL',
                    timeout: 1000
                })
                .then((r) => {
                    expect(r.data.results).to.have.lengthOf(3);
                    done();
                })
                .catch(function(obj) {
                    console.error(obj);
                    done(obj);
                });
        });

        it('should return results for A', function(done) {
            const nonce = Math.floor(new Date().getTime() / 1000);

            const pio = PRIMED({
                nonce: nonce,
                publicKey: publicKey,
                signature: sha512(publicKey + "" + secretKey + "" + nonce),
                connectionString: connectionString
            });

            pio.personalise({
                    campaign: "mycampaign",
                    signals: {
                        device: "A"
                    },
                    limit: 3,
                    timeout: 1000
                })
                .then((r) => {
                    expect(r.data.results).to.have.lengthOf(3);
                    done();
                })
                .catch(function(obj) {
                    console.error(obj);
                    done(obj);
                });
        });
    });

    describe('should perform conversion calls', function() {
        it('should accept conversion without payload', function(done) {
            const nonce = Math.floor(new Date().getTime() / 1000);

            const pio = PRIMED({
                nonce: nonce,
                publicKey: publicKey,
                signature: sha512(publicKey + "" + secretKey + "" + nonce),
                connectionString: connectionString
            });

            pio.convert({
                    ruuid: 'myruuid'
                })
                .then((r) => {
                    done();
                });
        });

        it('should accept conversion with payload', function(done) {
            const nonce = Math.floor(new Date().getTime() / 1000);

            const pio = PRIMED({
                nonce: nonce,
                publicKey: publicKey,
                signature: sha512(publicKey + "" + secretKey + "" + nonce),
                connectionString: connectionString
            });

            pio.convert({
                    ruuid: 'myruuid',
                    data: {
                        hearbeat: 10
                    }
                })
                .then((r) => {
                    done();
                });
        });
    });
});