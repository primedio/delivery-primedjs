.PHONY: tag release build push

GITHASH := `git rev-parse --short=10 HEAD`

release: tag build push

tag:
	gsed -i 's/self.COMMIT_HASH = ".*"/self.COMMIT_HASH = "'${GITHASH}'"/g' src/primedio.js

build:
	gulp build

push:
	gsutil cp build/*.js gs://cdn.primed.io/2.1.0-stable/

dev-build-release:
	docker build -f Dockerfile.server -t eu.gcr.io/primedio/primedjs-server:latest . && docker push eu.gcr.io/primedio/primedjs-server:latest
