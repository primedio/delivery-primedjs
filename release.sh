#!/usr/bin/env bash
if [ -z $1 ]; then echo "you must provide a major version, e.g.: 2.0"; exit 1; fi
if [ -z $2 ]; then echo "you must provide a minor version, e.g.: 2.0.1"; exit 1; fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

grunt publish