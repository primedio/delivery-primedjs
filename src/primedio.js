/*jshint esversion: 6 */

//     primedio.js 2.1.0
//     https://www.primed.io
//     (c) 2017 Matthijs van der Kroon, PrimedIO
//     primedjs may be freely distributed under the MIT license.


var PRIMED = (function() {
    const self = {};
    self.COMMIT_HASH = "46a3bc485c";
    self.VERSION = '2.1.0';
    self.tracker = { enabled: false };

    const __sid = retrieveSid(); // identifies a single pageview
    const __did = retrieveDid(); // identifies a single device throughout pageviews

    /* Enables management of features through bind(), all() and get().
     * 
     * ```
     * pio.features.bind({loc: function() {return 'latlon'; });
     * pio.features.get('userId');
     * pio.features.all();
     * ```
     */
    self.features = {
        __featureMap: {
            now: function() {
                return new Date();
            },
            ua: navigator.userAgent,
            sid: __sid,
            did: __did,
            uri: window.location.href,
            screenWidth: screen.width,
            screenHeight: screen.height,
            viewPortWidth: window.innerWidth,
            viewPortHeight: window.innerHeight,
            screenX: window.screenX,
            screenY: window.screenY,
            apikey: self.publicKey,
            location: null
        },
        bind: function(definition) {
            Object.assign(self.features.__featureMap, definition);

            return self.features;
        },
        all: function() {
            const __featureMap = {};
            for (const key of Object.keys(self.features.__featureMap)) {
                __featureMap[key] = self.features.get(key);
            }
            return __featureMap;
        },
        get: function(key) {
            if (key in self.features.__featureMap) {
                const value = self.features.__featureMap[key];

                if (value instanceof Function) {
                    return value();
                } else {
                    return value;
                }
            } else {
                throw new Error('no feature found for key: ' + key);
            }
        }
    };

    /* Enables management of detectors through bind(), all() and get().
     * 
     * ```
     * pio.detectors.bind({userid: 'myuserid'});
     * pio.detectors.bind({userid: function() {return new Date();} });
     * pio.detectors.get('userId');
     * pio.detectors.all();
     * ```
     */
    self.detectors = {
        __detectorMap: {},
        __args: {},
        bind: function(definition) {
            for (const key of Object.keys(definition)) {
                const value = definition[key];

                if (value instanceof Function) {
                    // store the arguments as expected by the detector
                    // in a dedicated `__args` datastructure
                    self.detectors.__args[key] = getArgs(value);
                }

                self.detectors.__detectorMap[key] = value;
            }

            return self.detectors;
        },
        all: function() {
            const __detectorMap = {};

            for (const key of Object.keys(self.detectors.__detectorMap)) {
                __detectorMap[key] = self.detectors.get(key);
            }

            return __detectorMap;
        },
        get: function(key) {
            if (key in self.detectors.__detectorMap) {
                const value = self.detectors.__detectorMap[key];

                if (value instanceof Function) {
                    // find args belonging to this value, and
                    // use these args to pull in features from
                    // feature set
                    let args = self.detectors.__args[key].map(function(f) {return self.features.get(f);});

                    return value.apply(this, args);
                } else {
                    return value;
                }
            } else {
                throw new Error('no detector found for key: ' + key);
            }
        }
    };

    /* Enables management of campaigns through bind(), all() and get().
     * 
     * ```
     * pio.campaigns.bind({
     *     key: 'mycampaign', 
     *     limit: 10, 
     *     map: function({ value, rank, ruuid, el, result }) {...} 
     * });
     *
     * pio.campaigns.get('mycampaign');
     * pio.campaigns.all();
     * ```
     */
    self.campaigns = {
        _campaignMap: {},
        bind: function({
            key,
            limit = 5,
            abvariant,
            map
        }) {
            self.campaigns._campaignMap[key] = {
                map: map,
                limit: limit,
                abvariant: abvariant
            };
            return self.campaigns;
        },
        all: function() {
            const _campaignMap = {};

            for (const key of Object.keys(self.campaigns._campaignMap)) {
                _campaignMap[key] = self.campaigns.get(key);
            }

            return _campaignMap;
        },
        get: function(key) {
            if (key in self.campaigns._campaignMap) {
                return self.campaigns._campaignMap[key];
            } else {
                throw new Error('no campaign found for key: ' + key);
            }
        }
    };

    /*
     * # PRIVATE METHODS
     */
    function getArgs(func) {
        const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
        const ARGUMENT_NAMES = /([^\s,]+)/g;

        const mapStr = func.toString().replace(STRIP_COMMENTS, '');
        let result = mapStr.slice(mapStr.indexOf('(') + 1, mapStr.indexOf(')')).match(ARGUMENT_NAMES);
        if (result === null)
            result = [];
        return result;
    }


    /**
     * Deep merge two objects.
     * @param target
     * @param ...sources
     */
    // function mergeDeep(target, ...sources) {

    //     /**
    //      * Simple object check.
    //      * @param item
    //      * @returns {boolean}
    //      */
    //     function isObject(item) {
    //         return (item && typeof item === 'object' && !Array.isArray(item));
    //     }


    //     if (!sources.length) return target;
    //     const source = sources.shift();

    //     if (isObject(target) && isObject(source)) {
    //         for (const key in source) {
    //             if (isObject(source[key])) {
    //                 if (!target[key]) Object.assign(target, {
    //                     [key]: {}
    //                 });
    //                 mergeDeep(target[key], source[key]);
    //             } else {
    //                 Object.assign(target, {
    //                     [key]: source[key]
    //                 });
    //             }
    //         }
    //     }

    //     return mergeDeep(target, ...sources);
    // }

    function mergeDeep(obj1, obj2) {

        for (var p in obj2) {
            try {
                // Property in destination object set; update its value.
                if (obj2[p].constructor == Object) {
                    obj1[p] = mergeDeep(obj1[p], obj2[p]);

                } else {
                    obj1[p] = obj2[p];

                }

            } catch (e) {
                // Property in destination object not set; create it and set its value.
                obj1[p] = obj2[p];

            }
        }

        return obj1;
    }

    function uuidv4() {
        // found here: https://stackoverflow.com/a/2117523
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function(c) {
                return (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16);
            }
        );
    }

    function constructLocation({
        connectionString,
        apiVersion,
        path,
        params = {}
    }) {
        var loc = connectionString + "/api/v" + apiVersion + "/" + path;
        if (Object.keys(params).length > 0) {
            loc = connectionString + "/api/v" + apiVersion + "/" + path + "?" + dictToQueryParams(params);
        }
        return loc.replace(/(https?:\/\/)|(\/){2,}/g, "$1$2"); // clean up excess slashes 
    }

    function retrieveSid() {
        var pioSid = sessionStorage.getItem('pioSid');

        if (pioSid !== null) {
            sessionStorage.setItem('pioFirstView', false);
            return pioSid;
        } else {
            const sid = uuidv4();
            sessionStorage.setItem('pioSid', sid);
            sessionStorage.setItem('pioFirstView', true);
            return sid;
        }
    }

    function retrieveDid() {
        if (localStorage.pioDid) {
            return localStorage.pioDid;
        } else {
            const did = uuidv4();
            localStorage.pioDid = did;
            return did;
        }
    }

    function loc(url) {
        var l = document.createElement("a");
        l.href = url;
        return l;
    }

    function hmacHeaders({
        publicKey,
        signature,
        nonce
    }) {

        return {
            'X-Authorization-Key': publicKey,
            'X-Authorization-Signature': signature,
            'X-Authorization-Nonce': nonce + ""
        };
    }

    function dictToQueryParams(dict) {
        function shim(v) {
            if (v !== null && typeof v === 'object') {
                return JSON.stringify(v);
            } else {
                return v;
            }
        }

        return Object
            .entries(dict)
            .map(
                function([k, v]) {
                    return `${encodeURIComponent(k)}=${encodeURIComponent(shim(v))}`;
                }
            ).join('&');
    }

    function requestJson({
        method = 'GET',
        url,
        headers = {},
        passthrough = {},
        timeout = 2000,
        data = {},
        customEvent
    }) {

        return new Promise(function(resolve, reject) {
            const xhr = new XMLHttpRequest();
            xhr.timeout = 2000;
            xhr.overrideMimeType("application/json");
            xhr.onreadystatechange = function(e) {
                if (xhr.readyState === 4) {
                    // https://stackoverflow.com/questions/12761255/can-xhr-trigger-onreadystatechange-multiple-times-with-readystate-done/13585135
                    this.onreadystatechange = null;

                    if (xhr.status === 200) {
                        
                        const data = JSON.parse(xhr.responseText);

                        if (customEvent !== undefined) {
                            const evt = new CustomEvent(customEvent.name, { detail: customEvent.map(data) });
                            document.dispatchEvent(evt);
                        }
                        
                        resolve(Object.assign({
                            data: data
                        }, passthrough));
                    } else {
                        
                        reject(Object.assign({
                            status: xhr.status
                        }, passthrough));
                    }
                }
            };
            xhr.ontimeout = function() {
                var retval = Object.assign({
                    status: 'timeout'
                }, passthrough);
                reject(retval);
            };

            xhr.open(method, url, true);

            for (var key in headers) {
                if (headers.hasOwnProperty(key)) {
                    xhr.setRequestHeader(key, headers[key]);
                }
            }
            xhr.setRequestHeader("Content-Type", "application/json");

            if (data !== undefined & method === 'POST') {
                xhr.send(JSON.stringify(data));
            } else {
                xhr.send();
            }
        });
    }

    function setupTracking({
        connectionString,
        geolocationEnabled,
        customBasicProperties = {},
        viewportTracking,
        events
    }) {
        // The connection URL has the following format:
        //     http[s]://<domain>:<port>[/<namespace>]
        // console.log("connecting ws to " + connectionString + "/v1")
        const socket = io.connect(connectionString + '/v1', { transports: ['websocket'], upgrade: false });

        const eventProxy = function({ eventType, eventObject }) {
            socket.emit("event", {
                apikey: self.publicKey,
                ts: (new Date()).getTime(),
                sid: __sid,
                did: __did,
                source: "BROWSER",
                sdkId: 0,
                sdkVersion: self.VERSION,
                type: eventType,
                customProperties: customBasicProperties,
                eventObject: eventObject
            });
        };

        self.tracker.track = eventProxy;

        if (events.POSITIONCHANGE.enabled) {
            const options = {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            };

            const success = function(pos) {
                const loc = {
                    latitude: pos.coords.latitude,
                    longitude: pos.coords.longitude,
                    accuracy: parseFloat(pos.coords.accuracy)
                };


                eventProxy({
                    eventType: "POSITIONCHANGE",
                    eventObject: loc
                });

                self.features.bind({
                    location: loc
                });

                document.dispatchEvent(new CustomEvent("pio.positionchange", {
                    detail: loc
                }));
            };

            if ("geolocation" in navigator) {
                navigator.geolocation.watchPosition(success, function(err) {}, options);
            }
        }

        if (events.START.enabled) {
            // send out 'init' event logging features and signals
            socket.on("connect", function() {

                if (sessionStorage.getItem('pioFirstView') == "true") {
                    const standard_features = ["now", "ua", "uri", "screenWidth", "screenHeight", "viewPortWidth", "viewPortHeight", "screenX", "screenY", "location"];
                    const excluded_features = ["sid", "did"];

                    const eventObject = {};
                    const allFeatures = self.features.all();
                    for (const key of Object.keys(allFeatures)) {
                        if (standard_features.indexOf(key) > -1) {
                            eventObject[key] = allFeatures[key];
                        }
                    }

                    customProperties = {};
                    Object.entries(events.START.customProperties).forEach(
                        function([key, value]) {
                            if (value instanceof Function) {
                                value = value.call(this);
                            }

                            customProperties[key] = value;
                        }
                    );

                    eventObject["customProperties"] = customProperties;

                    eventProxy({
                        eventType: "START",
                        eventObject: eventObject
                    });
                }
            });
        }

        if (events.VIEW.enabled) {
            window.addEventListener(events.VIEW.on, function(event) {
                customProperties = {};
                Object.entries(events.VIEW.customProperties).forEach(
                    function([key, value]) {
                        if (value instanceof Function) {
                            value = value.call(this);
                        }

                        customProperties[key] = value;
                    }
                );

                eventProxy({
                    eventType: "VIEW",
                    eventObject: {
                        'uri': event.newURL,
                        'customProperties': mergeDeep(event.detail || {}, customProperties || {})
                    }
                });
            }, false);
        }

        if (events.CLICK.enabled) {
            document.onclick = function(event) {
                eventProxy({
                    eventType: "CLICK",
                    eventObject: {
                        'x': event.clientX,
                        'y': event.clientY,
                        'interactionType': "LEFT"
                    }
                });
            };
        }

        if (events.MOUSEMOVE.enabled) {
            var _onmousemove_t1 = (new Date()).getTime();
            var _onmousemove_t2 = (new Date()).getTime();

            document.onmousemove = function(event) {

                if (_onmousemove_t2 - _onmousemove_t1 > events.MOUSEMOVE.samplerate) {

                    eventProxy({
                        eventType: "MOUSEMOVE",
                        eventObject: {
                            'x': event.clientX,
                            'y': event.clientY
                        }
                    });

                    _onmousemove_t1 = (new Date()).getTime();
                } else {
                    _onmousemove_t2 = (new Date()).getTime();
                }

            };
        }

        if (events.SCROLL.enabled) {
            var lastScrollTop = 0;

            window.onscroll = function(event) {

                var st = window.pageYOffset || document.documentElement.scrollTop;

                var direction = "UP";
                if (st > lastScrollTop) {
                    direction = "DOWN";
                }

                distance = lastScrollTop - st;

                lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling

                eventProxy({
                    eventType: "SCROLL",
                    eventObject: {
                        'direction': direction,
                        'distance': distance
                    }
                });

            };
        }

        if (events.HEARTBEAT.enabled) {
            // interval function that emits hearbeat
            var i = 0;

            // check if this is an existing session (so we counter
            // incrementing session counter)
            if (sessionStorage.getItem('pioHeartBeatCounter') !== null) {
                i = parseInt(sessionStorage.getItem('pioHeartBeatCounter'));
            }

            window.setInterval(function() {
                eventProxy({
                    eventType: "HEARTBEAT",
                    eventObject: {
                        'i': i
                    }
                });

                sessionStorage.setItem('pioHeartBeatCounter', i);

                i++;
            }, events.HEARTBEAT.interval);
        }

        if (events.END.enabled) {
            window.onbeforeunload = function() {
                eventProxy({
                    eventType: "END",
                    eventObject: {}
                });
            };
        }

        if (events.PERSONALISE.enabled) {
            document.addEventListener('personalise', function(event) {
                eventProxy({
                    eventType: "PERSONALISE",
                    eventObject: { "guuid": event.detail.guuid }
                });
            }, false);
        }

        if (events.CONVERT.enabled) {
            document.addEventListener('convert', function(event) {
                eventProxy({
                    eventType: "CONVERT",
                    eventObject: event.detail
                });
            }, false);
        }

        if (
            self.tracker.config.viewportTracking.enabled &&
            (
                events.ENTERVIEWPORT.enabled &&
                events.ENTERVIEWPORT.on) || (
                events.EXITVIEWPORT.enabled &&
                events.EXITVIEWPORT.on
            )
        ) {

            // only attach this eventhandler if the user has specified to enable viewportTracking ánd
            // the enterViewport event is enabled and has a trigger ('on') ór the exitViewport event
            // is enabled and has a trigger ('on').
            window.onscroll = function() {
                var elements = document.getElementsByClassName(viewportTracking.trackClass);

                Array.prototype.forEach.call(elements, function(element) {
                    detectElementVisibility({ element: element });
                });
            };
        }

        if (events.ENTERVIEWPORT.enabled && events.ENTERVIEWPORT.on) {

            window.addEventListener(events.ENTERVIEWPORT.on, function(event) {

                eventProxy({
                    eventType: "ENTERVIEWPORT",
                    eventObject: {
                        'customProperties': event.detail || {}
                    }
                });
            }, false);
        }

        if (events.EXITVIEWPORT.enabled && events.EXITVIEWPORT.on) {
            window.addEventListener(events.EXITVIEWPORT.on, function(event) {

                eventProxy({
                    eventType: "EXITVIEWPORT",
                    eventObject: {
                        'customProperties': event.detail || {}
                    }
                });
            }, false);
        }

        Object.entries(events.CUSTOM).forEach(
            function([key, value]) {
                if (value.enabled) {
                    var eventType = key.toUpperCase();

                    if ("eventType" in value) {
                        eventType = value.eventType;
                    }

                    window.addEventListener(value.on, function(event) {
                        eventProxy({
                            eventType: eventType,
                            eventObject: event.detail || {}
                        });
                    }, false);
                }
            }
        );

        const detectElementVisibility = function({ element }) {
            var rect = element.getBoundingClientRect();

            var visible = (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
                rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
            );

            if (visible) {
                const customProperties = {};
                Object.entries(events.ENTERVIEWPORT.customProperties).forEach(
                    function([key, value]) {
                        if (value instanceof Function) {
                            value = value.call(this, element);
                        }

                        customProperties[key] = value;
                    }
                );

                if (element.__seen__ === undefined || element.__seen__ === false) {
                    window.dispatchEvent(
                        new CustomEvent(events.ENTERVIEWPORT.on, {
                            detail: customProperties
                        })
                    );
                }
                element.__seen__ = true;
            } else if (element.__seen__) {
                element.__seen__ = false;

                const customProperties = {};
                Object.entries(events.EXITVIEWPORT.customProperties).forEach(
                    function([key, value]) {
                        if (value instanceof Function) {
                            value = value.call(this, element);

                        }

                        customProperties[key] = value;
                    }
                );

                window.dispatchEvent(
                    new CustomEvent(events.EXITVIEWPORT.on, {
                        detail: customProperties
                    })
                );
            }

            return visible;
        };

        self.tracker.attachMutationObserver = function({ trackClass, parent = (document.body || document.documentElement) }) {
            MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

            // define a new observer
            const obs = new MutationObserver(function(mutations, observer) {
                mutations.forEach(function(mutation) {
                    mutation.addedNodes.forEach(function(addedNode) {
                        try {
                            if (addedNode.className && addedNode.className.includes(trackClass)) {
                                detectElementVisibility({ element: addedNode });
                            }
                        } catch (err) {

                        }
                    });
                });
            });

            // have the observer observe foo for changes in children
            obs.observe(parent, {
                childList: true,
                subtree: true
            });

        };
    }

    /*
     * # PUBLIC METHODS
     */

    /* test-code */
    self.__testonly__ = {};
    self.__testonly__.getArgs = getArgs;
    self.__testonly__.dictToQueryParams = dictToQueryParams;
    self.__testonly__.uuidv4 = uuidv4;
    self.__testonly__.mergeDeep = mergeDeep;
    /* end-test-code */

    /**
     * Call to enable location tracking for this session
     *
     * @param {boolean} enableHighAccuracy default:true
     * @param {number} timeout default:5000
     * @param {number} maximumAge default:0
     *
     * ```
     * pio.track_location({});
     * ```
     */
    self.track_location = function({
        enableHighAccuracy = true,
        timeout = 5000,
        maximumAge = 0
    }) {
        const options = {
            enableHighAccuracy: enableHighAccuracy,
            timeout: timeout,
            maximumAge: maximumAge
        };

        const success = function(pos) {
            const loc = {
                latitude: pos.coords.latitude,
                longitude: pos.coords.longitude,
                accuracy: parseFloat(pos.coords.accuracy)
            };

            self.tracker.track({
                eventType: "POSITIONCHANGE",
                eventObject: loc
            });

            self.features.bind({
                location: loc
            });

            document.dispatchEvent(new CustomEvent("pio.positionchange", {
                detail: loc
            }));
        };

        if ("geolocation" in navigator) {
            navigator.geolocation.watchPosition(success, function(err) {}, options);
        }
    };

    /**
     * Call personalise endpoint directly
     * @param {string} campaign campaign key for which to obtain personalised results: `mycampaign`
     * @param {Object} signals Object of signal key-value pairs: `{userId: 'someuser'}`
     * @param {number} limit number of results that will be returned: `10`, defaults to `5`
     * @param {string} abvariant force calling results for a given abvariant: `__CONTROL__`
     *
     * ```
     * pio.personalise({ 
     *     campaign: 'mycampaign', 
     *     signals = {
     *         userId: 'myuserid'
     *     }, 
     *     limit=10, 
     *     abvariant='__CONTROL__'
     * });
     * ```
     */
    self.personalise = function({
        campaign,
        signals = {},
        limit = 5,
        abvariant,
        timeout,
        include_from_features = ["did", "sid"]
    }) {

        if (!(signals instanceof Object)) throw new Error('`signals` should be an object, you provided: ' + (typeof signals));
        if (typeof campaign !== 'string') throw new Error('`campaign` should be a String, you provided: ' + (typeof campaign));
        if ((abvariant !== undefined) && (typeof abvariant !== 'string')) throw new Error('`abvariant` should be a String, you provided: ' + (typeof abvariant));
        if ((timeout !== undefined) && (typeof timeout !== 'number')) throw new Error('`timeout` should be a Number, you provided: ' + (typeof timeout));
        
        for (let i = 0; i < include_from_features.length; i++) {
            const skey = include_from_features[i];
            const sval = self.features.get(skey);
            
            signals[skey] = sval;
        }
        
        const params = {
            "campaign": campaign,
            "signals": JSON.stringify(signals),
            "limit": limit
        };

        if (abvariant !== undefined) {
            params.abvariant = abvariant;
        }

        if (timeout !== undefined) {
            params.timeout = parseInt(timeout);
        }

        const url = constructLocation({
            connectionString: self.connectionString,
            apiVersion: self.apiVersion,
            path: 'personalise',
            params: params
        });

        // console.log(`personalising for ${campaign}, using ${JSON.stringify(signals)}`);

        return requestJson({
            url: url,
            headers: hmacHeaders({
                publicKey: self.publicKey,
                signature: self.signature,
                nonce: self.nonce
            }),
            passthrough: {
                cmp: campaign,
                signals: signals
            },
            customEvent: {
                'name': 'personalise',
                'map': function(d) {
                    return d;
                }
            }
        });
    };

    /**
     * Mark a result as having converted using the result uuid (`ruuid`)
     * @param {string} ruuid result uuid for which the system should mark the conversion
     * @param {Object} data optional data payload
     *
     * ```
     * pio.convert({ ruuid: '...', data = {heartbeat: 10}});
     * ```
     */
    self.convert = function({
        ruuid,
        data = {}
    }) {
        if (typeof ruuid !== 'string') throw new Error('`ruuid` should be a String, you provided: ' + (typeof ruuid));
        if ((data !== undefined) && !(data instanceof Object)) throw new Error('`data` should be an object, you provided: ' + data);

        const url = constructLocation({
            connectionString: self.connectionString,
            apiVersion: self.apiVersion,
            path: 'conversion/' + ruuid
        });

        return requestJson({
            method: 'POST',
            headers: hmacHeaders({
                publicKey: self.publicKey,
                signature: self.signature,
                nonce: self.nonce
            }),
            url: url,
            data: data,
            customEvent: {
                'name': 'convert',
                'map': function(d) {
                    return { ruuid: ruuid, hasData: Object.keys(data).length !== 0 };
                }
            }
        });
    };


    const verifyBoundElements = function() {
        // verify that each html el with a $pio prefix is in fact bound
        const elements = document.querySelectorAll("[id^='" + self.prefix + "']");
        for (let i = 0; i < elements.length; i++) {
            const idx = elements[i].id.indexOf(".");
            const cmpname = elements[i].id.slice(idx + 1);

            if (!(cmpname in self.campaigns.all())) {
                console.warn(`found unbound element with id: '${elements[i].id}', try:
    pio.bind({
        campaign: 'dummy.frontpage.recommendations',
        map: function({value, rank, el, cmp, signals, experiment, guuid }) {...}
    });
                    `);
            }
        }
    };

    const renderResult = function(result) {
        if (result.data.results.length == 0) {
            console.warn("personalise call for " + result.cmp + " and " + result.data.experiment.abvariant.label + " returned 0 results");
        } else {
            console.log("personalise call num results: " + result.data.results.length + ", abvariant: " + result.data.experiment.abvariant.label);
        }

        for (let ri = 0; ri < result.data.results.length; ri++) {
            const el = document.getElementById(self.prefix + '.' + result.cmp);

            self.campaigns.get(result.cmp).map({
                value: result.data.results[ri].target.value,
                rank: ri,
                el: el,
                ruuid: result.data.results[ri].ruuid,
                result: result.data.results[ri]
            });
        }
    };

    window.onload = function() {

        if (self.tracker.enabled && self.tracker.config.viewportTracking.enabled) {
            self.tracker.attachMutationObserver(self.tracker.config.viewportTracking);
        }

        verifyBoundElements();

        // for each bound campaign
        for (const cmp in self.campaigns.all()) {
            // get signals
            const signals = self.detectors.all();

            // call personalise
            const promise = self.personalise({
                campaign: cmp,
                signals: signals,
                limit: self.campaigns.get(cmp).limit
            });

            promise
                .then(renderResult)
                .catch(console.error);
        }
    };

    /**
     * Instantiate PrimedIO API wrapper
     * @param {string} connectionString connection string as provided by PrimedIO
     * @param {string} publicKey public key as provided by PrimedIO
     * @param {string} nonce UTC timestamp in seconds from midnight 1970
     * @param {string} signature server side generate signature using `nonce` and `publicKey`
     *
     * ```
     * pio = PRIMED({
     *     connectionString: 'https://primed.io:443/api/v1', 
     *     publicKey: '...', 
     *     nonce: 123456789, 
     *     signature: '...', 
     *     tracking: {
     *         connectionString: 'https://primed.io:443/track'
     *     }
     * });
     * ```
     */
    return function({
        connectionString,
        apiVersion = 1,
        publicKey,
        nonce,
        signature,
        tracking = {},
        prefix = '$pio'
    }) {
        if (typeof connectionString !== 'string') throw new Error('`connectionString` should be a String, you provided: ' + (typeof connectionString));
        if (typeof apiVersion !== 'number') throw new Error('`apiVersion` should be a Number, you provided: ' + (typeof apiVersion));
        if (typeof publicKey !== 'string') throw new Error('`publicKey` should be a String, you provided: ' + (typeof publicKey));
        if (typeof signature !== 'string') throw new Error('`signature` should be a String, you provided: ' + (typeof signature));
        if (typeof nonce !== 'number') throw new Error('`nonce` should be a Number, you provided: ' + (typeof nonce));
        if (!(tracking instanceof Object)) throw new Error('`tracking` should be an object, you provided: ' + (typeof tracking));

        self.connectionString = connectionString;
        self.apiVersion = apiVersion;
        self.publicKey = publicKey;
        self.nonce = nonce;
        self.signature = signature;
        self.prefix = prefix;

        Date.prototype.toJSON = function() {
            /*
             * Ensures that we output datetimes as ISO8601
             * Strings rather then UTC: this is because we
             * want to converse local timezone information
             */
            var tzo = -this.getTimezoneOffset(),
                dif = tzo >= 0 ? '+' : '-',
                pad = function(num) {
                    var norm = Math.floor(Math.abs(num));
                    return (norm < 10 ? '0' : '') + norm;
                };
            return this.getFullYear() +
                '-' + pad(this.getMonth() + 1) +
                '-' + pad(this.getDate()) +
                'T' + pad(this.getHours()) +
                ':' + pad(this.getMinutes()) +
                ':' + pad(this.getSeconds()) +
                dif + pad(tzo / 60) +
                ':' + pad(tzo % 60);
        };

        if ('connectionString' in tracking) {
            self.tracker.enabled = true;

            self.tracker.config = {
                connectionString: tracking.connectionString,
                geolocationEnabled: false,
                customBasicProperties: tracking.customBasicProperties || {},
                viewportTracking: mergeDeep({
                    enabled: true,
                    trackClass: "__trackInView__"
                }, tracking.viewportTracking || {}),
                events: mergeDeep({
                    "MOUSEMOVE": { enabled: false, samplerate: 100 },
                    "CLICK": { enabled: false },
                    "VIEW": { enabled: true, customProperties: {}, on: "hashchange" },
                    "SCROLL": { enabled: false },
                    "ENTERVIEWPORT": {
                        enabled: false,
                        customProperties: {
                            ruuid: function(element) {
                                return element.getAttribute("ruuid") || null;
                            }
                        },
                        on: "enterviewport"
                    },
                    "EXITVIEWPORT": {
                        enabled: false,
                        customProperties: {
                            ruuid: function(element) {
                                return element.getAttribute("ruuid") || null;
                            }
                        },
                        on: "exitviewport"
                    },
                    "HEARTBEAT": { enabled: true, interval: 10000 },
                    "START": { enabled: true, customProperties: {} },
                    "END": { enabled: true },
                    "PERSONALISE": { enabled: true },
                    "CONVERT": { enabled: true },
                    "POSITIONCHANGE": { enabled: false },
                    "CUSTOM": {}
                }, tracking.events || {})
            };

            setupTracking(self.tracker.config);
        }

        return self;
    };
}());