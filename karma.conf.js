// Karma configuration
// Generated on Thu May 03 2018 12:43:28 GMT+0200 (CEST)

module.exports = function (config) {
  config.set({
    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: "",

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ["mocha", "chai"],

    plugins: [
      "karma-chrome-launcher",
      "karma-firefox-launcher",
      "karma-mocha",
      "karma-chai",
      "karma-env-preprocessor",
    ],

    // list of files / patterns to load in the browser
    files: [
      { pattern: "lib/**/*.js" },
      { pattern: "src/**/*.js" },
      { pattern: "test/**/*.spec.js" },
    ],

    // list of files / patterns to exclude
    exclude: [],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      "**/*.js": ["env"],
    },

    envPreprocessor: [
      "PRIMEDIO_CONNECTIONSTRING",
      "PRIMEDIO_APIKEY_SECRETKEY",
      "PRIMEDIO_APIKEY_PUBKEY",
      "PRIMEDIO_AUTH_USERNAME",
      "PRIMEDIO_AUTH_PASSWORD",
    ],

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ["progress"],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    // browsers: ['Firefox_Headless'],
    browsers: ["FirefoxHeadless"],

    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: "ChromeHeadless",
        flags: [
          "--no-sandbox", // required to run without privileges in docker
          "--user-data-dir=/tmp/chrome-test-profile",
          "--disable-web-security",
          "--disable-setuid-sandbox",
        ],
      },
      Firefox_Headless: {
        base: "Firefox",
        flags: ["-headless"],
      },
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,
  });
};
